package com.automationedge.integration.external;

import java.util.List;

import com.automationedge.integration.model.AutomationStatusResult;
import com.automationedge.model.AutomationRequest;
import com.automationedge.model.AutomationResponse;

/**
 * @author aishwaryam
 *
 */
public interface IAEIntegrationFramework {
	
	public Object doAuthenticate(String[] args);
	
	public List<AutomationRequest> getAutomationRequests(Object authenticatedObject, String[] args);
	
	public void notifyAutomationRequestStatus(Object authenticatedObject, String[] args, List<AutomationStatusResult> statusList);
	
	public void updateResponse(Object authenticatedObject, String[] args, List<AutomationResponse> automationResponseList);

}
