package com.automationedge.integration.model;

import com.automationedge.enums.WorkflowStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author aishwaryam
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AutomationStatusResult {
	@JsonIgnore
	private String source;
	
	private String sourceId;
	private String automationRequestId;
	private WorkflowStatus status; 
	private String message;
	
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getSourceId() {
		return sourceId;
	}
	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}
	public String getAutomationRequestId() {
		return automationRequestId;
	}
	public void setAutomationRequestId(String automationRequestId) {
		this.automationRequestId = automationRequestId;
	}
	public WorkflowStatus getStatus() {
		return status;
	}
	public void setStatus(WorkflowStatus status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AutomationStatusResult [source=");
		builder.append(source);
		builder.append(", sourceId=");
		builder.append(sourceId);
		builder.append(", automationRequestId=");
		builder.append(automationRequestId);
		builder.append(", status=");
		builder.append(status);
		builder.append(", message=");
		builder.append(message);
		builder.append("]");
		return builder.toString();
	}
	
}
