package com.automationedge.integration.util;

import java.io.IOException;
import java.io.InputStream;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;

/**
 * @author aishwaryam
 *
 */
public class JsonUtils {

	private static ObjectMapper objectMapper = new ObjectMapper();

	private JsonUtils() {
	}

	public static String serialize(Object object) throws JsonProcessingException {
		ObjectWriter objWriter = objectMapper.writer();
		return objWriter.writeValueAsString(object);
	}

	public static <T> T deserialize(String json, Class<T> returnType) throws IOException {
		ObjectReader objReader = objectMapper.readerFor(returnType);
		return objReader.readValue(json);
	}

	public static <T> T deserialize(String json, TypeReference<?> returnTypeReference) throws IOException {
		ObjectReader objReader = objectMapper.readerFor(returnTypeReference);
		return objReader.readValue(json);
	}

	public static <T> T deserialize(InputStream source, Class<T> returnType) throws IOException {
		ObjectReader objReader = objectMapper.readerFor(returnType);
		return objReader.readValue(source);
	}
}
